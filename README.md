Keley test
=====

A Symfony project created on June 30, 2016, 8:54 am.

# Installation #

Before the first launching of the application you should execute next steps:

* Install composer verdors: composer install
* Create a database
* Update the database structure: ```php app/console doctrine:schema:update --force```