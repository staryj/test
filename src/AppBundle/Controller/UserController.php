<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/users")
 */
class UserController extends FOSRestController
{
    /**
     * @Route("/")
     * @Method("GET")
     * @Rest\View
     * @ApiDoc(
     *   description="Retrieve list of users",
     *   resource=true
     * )
     */
    public function fetchAction()
    {
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();

        return $this->view($users);
    }

    /**
     * @Route("/")
     * @Method("POST")
     * @Rest\View
     * @ApiDoc(
     *   description="Create a user",
     *   resource=true,
     *   parameters={
     *     {"name"="email", "dataType"="email", "required"=true, "description"="Email"},
     *     {"name"="firstname", "dataType"="string", "required"=true, "description"="User's firstname"},
     *     {"name"="lastname", "dataType"="string", "required"=true, "description"="User's lastname"},
     *     {"name"="isActive", "dataType"="boolean", "required"=true, "description"="Is active"},
     *     {"name"="groupId", "dataType"="integer", "required"=true, "description"="Group id"},
     *   }
     * )
     */
    public function createAction(Request $request)
    {
        if (!($group = $this->isGroupExist($request->request->get('groupId')))) {
            return array(
                'success' => false,
                'errors' => 'Group does not exist'
            );
        }
        $request->request->remove('groupId');

        $form = $this->createForm('AppBundle\Form\UserType', new User(), array(
            'csrf_protection' => false
        ));

        $form->submit($request->request->all());
        if ($form->isValid()) {
            $user = $form->getData();
            $user->setGroup($group);
            $this->get('app.class_manager')->flush($user);

            return $user;
        }

        return array(
            'success' => false,
            'errors' => $form->getErrors()
        );
    }

    /**
     * @Route("/{id}/", requirements={"id":"\d+"})
     * @Method("GET")
     * @Rest\View
     * @ParamConverter("user", class="AppBundle:User")
     * @ApiDoc(
     *   description="Fetch info of a user",
     *   resource=true
     * )
     */
    public function getUserAction(User $user)
    {
        return $user;
    }

    /**
     * @Route("/{id}/modify", requirements={"id":"\d+"})
     * @Method("POST")
     * @ParamConverter("user", class="AppBundle:User")
     * @Rest\View
     * @ApiDoc(
     *   description="Modify a user",
     *   resource=true,
     *   parameters={
     *     {"name"="email", "dataType"="email", "required"=false, "description"="Email"},
     *     {"name"="firstname", "dataType"="string", "required"=false, "description"="User's firstname"},
     *     {"name"="lastname", "dataType"="string", "required"=false, "description"="User's lastname"},
     *     {"name"="isActive", "dataType"="string", "required"=false, "description"="Is active"},
     *     {"name"="groupId", "dataType"="integer", "required"=false, "description"="Group id"},
     *   }
     * )
     */
    public function modifyUserAction(User $user, Request $request)
    {
        if (($email = $request->request->get('email')) !== null) {
            $user->setEmail($email);
        }

        if (($firstname = $request->request->get('firstname')) !== null) {
            $user->setFirstname($firstname);
        }

        if (($lastname = $request->request->get('lastname')) !== null) {
            $user->setLastname($lastname);
        }

        if (($isActive = $request->request->get('isActive')) !== null) {
            $user->setIsActive($request->request->get($isActive));
        }

        if ($group = $this->isGroupExist($request->request->get('groupId'))) {
            $user->setGroup($group);
        }

        $this->get('app.class_manager')->flush($user);

        return $user;
    }

    private function isGroupExist($groupId)
    {
        if ($groupId !== null && $group = $this->getDoctrine()->getRepository('AppBundle:Group')->find($groupId)
        ) {
            return $group;
        } else {
            return false;
        }
    }
}
