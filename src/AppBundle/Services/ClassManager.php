<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;

class ClassManager
{
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function flush($object)
    {
        $this->em->persist($object);
        $this->em->flush();
    }

    public function remove($object)
    {
        $this->em->remove($object);
        $this->em->flush();
    }

    public function getEntityManager()
    {
        return $this->em;
    }
}